import logging
import os
import re
import sys

import asyncio
import asyncssh


__log_level = 'DEBUG'

__config = {
    'publish_server_hostname': os.getenv('DEPLOY_SERVER_HOSTNAME'),
    'publish_server_username': os.getenv('DEPLOY_SERVER_USERNAME'),
    'deploy_dir_development': os.getenv('DEPLOY_DIR_DEVELOPMENT'),
    'deploy_dir_production': os.getenv('DEPLOY_DIR_PRODUCTION'),
}

logging.basicConfig(level=__log_level)
__logger = logging.getLogger(__name__)


async def main(config):
    global __logger
    async with asyncssh.connect(
        config['publish_server_hostname'],
        username=config['publish_server_username'],
        client_keys=[config['identity_file']]
    ) as conn:
        if os.getenv('BITBUCKET_TAG'):
            commands = [
                'cd {}'.format(config['deploy_dir_production']),
                'sudo git pull origin tag {}'.format(os.getenv('BITBUCKET_TAG')),
                'sudo git checkout -b {}-tag {}'.format(
                    os.getenv('BITBUCKET_TAG'),
                    os.getenv('BITBUCKET_TAG')),
            ]
        elif os.getenv('BITBUCKET_BRANCH') == 'master':
            commands = [
                'cd {}'.format(config['deploy_dir_development']),
                'sudo git pull origin master',
            ]
        else:
            return
        commands.extend([
            'docker-compose stop web',
            'docker-compose rm -f web',
            'docker-compose build web',
            'docker-compose up -d',
        ])
        command = ' && '.join(commands)
        result = await conn.run(
            command, check=True, stderr=asyncio.subprocess.STDOUT)
        print(result.stdout, end='')


if __name__ == '__main__':
    try:
        with open(os.path.join(os.getenv("HOME"), '.ssh/config')) as f:
            rx = re.compile(r'IdentityFile\s*(.*)')
            lines = f.readlines()
            for line in lines:
                m = rx.match(line)
                if m:
                    __config['identity_file'] = m.group(1)
                    break
        asyncio.get_event_loop().run_until_complete(main(__config))
    except (OSError, asyncssh.Error) as exc:
        sys.exit('SSH connection failed: ' + str(exc))
