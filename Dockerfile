FROM pypy:3.6-7.1.0-stretch
ENV PYTHONUNBUFFERED 1
EXPOSE 80
RUN apt-get update \
    && apt-get install -y --no-install-recommends libpq-dev git \
    && rm -rf /var/lib/apt/lists/* \
    && update-alternatives --install \
        /usr/bin/python python /usr/local/bin/pypy3 3 \
    && mkdir /chronicle
WORKDIR /chronicle
ADD requirements.txt /chronicle/
RUN pip install -r requirements.txt \
    && rm requirements.txt
ADD src /chronicle/
ADD docker-entrypoint.sh /chronicle
VOLUME [ "/data" ]
ENTRYPOINT [ "/chronicle/docker-entrypoint.sh" ]
