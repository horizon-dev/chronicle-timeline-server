import uuid

from django.conf import settings
from django.db import models


class UserPermissions(models.Model):
    create_user_account = models.BooleanField(default=False)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        editable=False,
        primary_key=True,
        related_name='permissions'
    )

    class Meta:
        verbose_name = "User permissions"
        verbose_name_plural = "User permissions"


class UserPublicKey(models.Model):
    public_key = models.BinaryField(default=b'')
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        editable=False,
        primary_key=True,
        related_name='public_key'
    )


class Timeline(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        editable=False,
        related_name='timelines')
    short_description = models.CharField(
        blank=True,
        default='',
        max_length=256)
    long_description = models.TextField(
        blank=True,
        default='')
    created_at = models.DateTimeField(
        auto_now_add=True,
        editable=False)
    last_entry = models.OneToOneField(
        'TimelineEntry',
        on_delete=models.CASCADE,
        blank=True,
        editable=False,
        null=True,
        related_name='timeline_last_entry'
    )


class TimelineEntry(models.Model):
    timeline = models.ForeignKey(
        Timeline,
        on_delete=models.CASCADE,
        editable=False,
        related_name='entries')
    mime_type = models.CharField(
        editable=False,
        max_length=256)
    created_at = models.DateTimeField(
        auto_now_add=True,
        editable=False)
    previous = models.OneToOneField(
        'self',
        on_delete=models.CASCADE,
        blank=True,
        editable=False,
        null=True,
        related_name='next')
    writable = models.BooleanField(
        default=False,
        editable=False
    )

    class Meta():
        verbose_name_plural = "Timeline entries"


class TimelineEntryMetadata(models.Model):
    timeline_entry = models.ForeignKey(
        TimelineEntry,
        on_delete=models.CASCADE,
        editable=False,
        related_name='metadata')
    key = models.CharField(max_length=256)
    value = models.CharField(max_length=256)

    class Meta:
        verbose_name_plural = "timeline entry metadata"


class AccessToken(models.Model):
    secret = models.CharField(max_length=64)
    description = models.CharField(max_length=256)
    script = models.TextField()
    state = models.TextField(blank=True)
    created_at = models.DateTimeField(
        auto_now_add=True,
        editable=False)

    class Meta:
        abstract = True


class TimelineAccessToken(AccessToken):
    timeline = models.ForeignKey(
        Timeline,
        on_delete=models.CASCADE,
        editable=False,
        related_name='access_tokens')
    permission_read = models.BooleanField(default=False)
    permission_read_entries = models.BooleanField(default=False)
    permission_edit_entries = models.BooleanField(default=False)
    permission_add_entry = models.BooleanField(default=False)
    permission_clone = models.BooleanField(default=False)


class TimelineEntryAccessToken(AccessToken):
    timeline_entry = models.ForeignKey(
        TimelineEntry,
        on_delete=models.CASCADE,
        editable=False,
        related_name='access_tokens')
    permission_read = models.BooleanField(default=False)
    permission_edit = models.BooleanField(default=False)
