from django.conf import settings
from django.contrib import auth
from django.db.models.signals import post_save
from django.dispatch import receiver

from . import models


@receiver(
    post_save,
    sender=models.TimelineEntry,
    dispatch_uid='signals.timeline_entry_saved_handler')
def timeline_entry_saved_handler(sender, instance, created, *args, **kwargs):
    if created:
        tl = instance.timeline
        instance.previous = tl.last_entry
        instance.save()
        tl.last_entry = instance
        tl.save()

@receiver(
    post_save,
    sender=auth.get_user_model(),
    dispatch_uid='signals.user_saved_handler')
def user_saved_handler(sender, instance, created, *args, **kwargs):
    if created:
        up = models.UserPermissions()
        up.user = instance
        up.save()
        upk = models.UserPublicKey()
        upk.user = instance
        upk.save()
