import io

from django.apps import apps
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import StreamingHttpResponse
from django.shortcuts import redirect
from django.views.generic.base import (TemplateResponseMixin, View)

from allauth.utils import get_request_param
from allauth.account.adapter import get_adapter
from allauth.account.utils import get_next_redirect_url

from . import forms
from . import models
from . import utils


class IndexView(TemplateResponseMixin, View):

    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        if 'create_timeline_form' in kwargs:
            create_timeline_form = kwargs['create_timeline_form']
        else:
            create_timeline_form = forms.CreateTimelineForm()
        return self.render_to_response({
            'create_timeline_form': create_timeline_form,
            'timelines': request.user.timelines.all(),
        })

    def post(self, request, *args, **kwargs):
        adapter = get_adapter(self.request)
        if request.POST['action'] == 'create_timeline':
            create_timeline_form = forms.CreateTimelineForm(request.POST)
            if create_timeline_form.is_valid():
                create_timeline_form.instance.user = request.user
                create_timeline_form.save()
                adapter.add_message(
                    self.request,
                    messages.SUCCESS,
                    'messages/timeline_created.txt')
            else:
                kwargs['create_timeline_form'] = create_timeline_form
        elif request.POST['action'] == 'delete_timeline':
            utl = models.Timeline.objects.get(id=request.POST['timeline_id'])
            utl.delete()
            adapter.add_message(
                self.request,
                messages.SUCCESS,
                'messages/timeline_deleted.txt')
        return self.get(request, *args, **kwargs)

index = login_required(IndexView.as_view())


class AccountDeleteView(TemplateResponseMixin, View):

    template_name = "account/delete.html"
    redirect_field_name = "next"

    def get(self, *args, **kwargs):
        return self.render_to_response(self.get_context_data())

    def post(self, *args, **kwargs):
        url = self.get_redirect_url()
        self.delete_account()
        return redirect(url)

    def delete_account(self):
        user = self.request.user
        adapter = get_adapter(self.request)
        adapter.add_message(
            self.request,
            messages.SUCCESS,
            'account/messages/account_deleted.txt')
        adapter.logout(self.request)
        user.delete()

    def get_context_data(self, **kwargs):
        ctx = kwargs
        redirect_field_value = get_request_param(self.request,
                                                 self.redirect_field_name)
        ctx.update({
            "redirect_field_name": self.redirect_field_name,
            "redirect_field_value": redirect_field_value})
        return ctx

    def get_redirect_url(self):
        return (
            get_next_redirect_url(
                self.request,
                self.redirect_field_name) or get_adapter(
                    self.request).get_logout_redirect_url(
                        self.request))

account_delete = login_required(AccountDeleteView.as_view())


class ProfileView(TemplateResponseMixin, View):

    template_name = 'account/profile.html'

    def get(self, request, *args, **kwargs):
        if 'public_key_form' in kwargs:
            public_key_form = kwargs['public_key_form']
        else:
            pk = request.user.public_key.public_key
            public_key_form = forms.AddOrUpdatePublicKeyForm(initial={
                'public_key': bytes(pk).decode('ascii')
            })

        return self.render_to_response({
            'public_key_form': public_key_form,
        })

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action', '')
        if action == 'update_public_key':
            form = forms.AddOrUpdatePublicKeyForm(request.POST)
            if form.is_valid():
                pk = request.user.public_key
                pk.public_key = form.cleaned_data['public_key'].encode('ascii')
                pk.save()
            else:
                kwargs['public_key_form'] = form

        return self.get(request, *args, **kwargs)

account_profile = login_required(ProfileView.as_view())


class TimelineView(TemplateResponseMixin, View):

    template_name = "timeline/index.html"

    def get(self, request, timeline_id=None, *args, **kwargs):
        utl = models.Timeline.objects.get(pk=timeline_id)
        create_entry_form = forms.CreateTimelineEntryForm()
        create_token_form = forms.CreateTimelineAccessTokenForm()
        clone_timeline_form = forms.CreateTimelineForm(instance=utl)
        if 'create_entry_form' in kwargs:
            create_entry_form = kwargs['create_entry_form']
        if 'create_token_form' in kwargs:
            create_token_form = kwargs['create_token_form']
        if 'clone_timeline_form' in kwargs:
            clone_timeline_form = kwargs['clone_timeline_form']
        return self.render_to_response({
            'timeline': utl,
            'create_entry_form': create_entry_form,
            'create_token_form': create_token_form,
            'clone_timeline_form': clone_timeline_form,
        })

    def post(self, request, timeline_id=None, *args, **kwargs):
        action = request.POST.get('action', '')
        utl = models.Timeline.objects.get(pk=timeline_id)
        fs = apps.get_app_config('server').filestore
        if action == 'add_entry':
            form = forms.CreateTimelineEntryForm(request.POST, request.FILES)
            if form.is_valid():
                te = None
                if form.cleaned_data.get('text_data'):
                    te = models.TimelineEntry(
                        timeline=utl,
                        mime_type='text/plain',
                        previous=utl.last_entry,
                    )
                    te.save()
                    fs.put(
                        utl.id,
                        te.id,
                        io.BytesIO(form.cleaned_data.get(
                            'text_data').encode('utf8')))
                else:
                    file_data = form.files.get('file_data')
                    te = models.TimelineEntry(
                        timeline=utl,
                        mime_type=file_data.content_type,
                        previous=utl.last_entry,
                    )
                    te.save()
                    fs.put(utl.id, te.id, file_data)
                for line in form.cleaned_data.get('metadata').splitlines():
                    p = line.split(':')
                    m = models.TimelineEntryMetadata()
                    m.key = p[0].strip()
                    m.value = p[1].strip()
                    m.timeline_entry = te
                    m.save()
            else:
                kwargs['create_entry_form'] = form
        elif action == 'create_token':
            form = forms.CreateTimelineAccessTokenForm(request.POST)
            if form.is_valid():
                at = models.TimelineAccessToken()
                at.timeline = utl
                at.description = form.cleaned_data.get('description')
                at.permission_read = form.cleaned_data.get('permission_read')
                at.permission_read_entries = \
                        form.cleaned_data.get('permission_read_entries')
                at.permission_add_entry = \
                        form.cleaned_data.get('permission_add_entry')
                at.permission_edit_entries = \
                        form.cleaned_data.get('permission_edit_entries')
                at.script = form.cleaned_data.get('script')
                at.state = form.cleaned_data.get('state')
                at.secret = utils.generate_secret_key()
                at.save()
            else:
                kwargs['create_token_form'] = form
        elif action == 'delete_token':
            at = models.TimelineAccessToken.objects.get(
                id=request.POST['token_id'])
            at.delete()
        elif action == 'clone_timeline':
            form = forms.CreateTimelineForm(request.POST)
            if form.is_valid():
                dest = utils.clone_timeline(utl, request.user)
                dest.short_description = form.instance.short_description
                dest.long_description = form.instance.long_description
                dest.save()
                adapter = get_adapter(self.request)
                adapter.add_message(
                    self.request,
                    messages.SUCCESS,
                    'timeline/messages/timeline_cloned.txt')
            else:
                kwargs['clone_timeline_form'] = form
        return self.get(request, timeline_id=timeline_id, *args, **kwargs)

timeline = login_required(TimelineView.as_view())


class TimelineEntryView(TemplateResponseMixin, View):

    template_name = "timeline/entry.html"

    def get(self, request, timeline_id=None, entry_id=None, *args, **kwargs):
        utl = models.Timeline.objects.get(pk=timeline_id)
        tle = models.TimelineEntry.objects.get(pk=entry_id, timeline_id=utl.id)
        if 'create_token_form' in kwargs:
            create_token_form = kwargs['create_token_form']
        else:
            create_token_form = forms.CreateTimelineEntryAccessTokenForm()
        return self.render_to_response({
            'timeline': utl,
            'entry': tle,
            'create_token_form': create_token_form,
        })

    def post(self, request, timeline_id=None, entry_id=None, *args, **kwargs):
        action = request.POST.get('action', '')
        utl = models.Timeline.objects.get(pk=timeline_id)
        tle = models.TimelineEntry.objects.get(pk=entry_id, timeline_id=utl.id)
        if action == 'create_token':
            form = forms.CreateTimelineEntryAccessTokenForm(request.POST)
            if form.is_valid():
                at = models.TimelineEntryAccessToken()
                at.timeline_entry = tle
                at.description = form.cleaned_data.get('description')
                at.permission_read = form.cleaned_data.get('permission_read')
                at.permission_edit = form.cleaned_data.get('permission_edit')
                at.script = form.cleaned_data.get('script')
                at.state = form.cleaned_data.get('state')
                at.secret = utils.generate_secret_key()
                at.save()
            else:
                kwargs['create_token_form'] = form
        elif action == 'delete_token':
            at = models.TimelineEntryAccessToken.objects.get(
                id=request.POST['token_id'])
            at.delete()
        return self.get(
            request,
            timeline_id=timeline_id,
            entry_id=entry_id,
            *args,
            **kwargs)

timeline_entry = login_required(TimelineEntryView.as_view())


@login_required
def timeline_entry_content(request, timeline_id=None, entry_id=None):
    utl = models.Timeline.objects.get(pk=timeline_id)
    tle = models.TimelineEntry.objects.get(pk=entry_id, timeline_id=utl.id)
    fs = apps.get_app_config('server').filestore
    f = fs.get(timeline_id, entry_id)
    return StreamingHttpResponse(f, content_type=tle.mime_type)
