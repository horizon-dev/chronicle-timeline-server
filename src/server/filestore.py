import os

from abc import ABC, abstractmethod

class AbstractFilestore(ABC):

    @abstractmethod
    def get(self, timeline_id, entry_id):
        pass

    @abstractmethod
    def put(self, timeline_id, entry_id, data):
        pass

    @abstractmethod
    def put_partial(self, timeline_id, entry_id, data, start, end):
        pass


class FilesystemFilestore(AbstractFilestore):

    def __init__(self, root):
        self.root = root

    def to_path(self, timeline_id, entry_id):
        return os.path.join(
            self.root, str(timeline_id), '{}.bin'.format(entry_id))

    def get(self, timeline_id, entry_id):
        return open(self.to_path(timeline_id, entry_id), 'rb')

    def put(self, timeline_id, entry_id, data):
        d = os.path.join(self.root, str(timeline_id))
        os.makedirs(d, exist_ok=True)
        with open(self.to_path(timeline_id, entry_id), 'wb') as f:
            f.write(data.read())

    def put_partial(self, timeline_id, entry_id, data, start, end):
        d = os.path.join(self.root, str(timeline_id))
        os.makedirs(d, exist_ok=True)
        file_path = self.to_path(timeline_id, entry_id)
        if os.path.getsize(file_path) < end:
            diff = end - os.path.getsize(file_path)
            with open(file_path, 'a+b') as f:
                f.write(b'\x00' * diff)
        with open(file_path, 'r+b') as f:
            f.seek(start, 0)
            f.write(data.read())
