from django import forms

from . import models


class AddOrUpdatePublicKeyForm(forms.Form):
    public_key = forms.fields.CharField(
        required=True,
        widget=forms.Textarea(attrs={ 'rows': 15, 'cols': 65 })
    )


class CreateTimelineForm(forms.ModelForm):
    class Meta:
        model = models.Timeline
        fields = ['short_description', 'long_description']


class CreateTimelineEntryForm(forms.Form):

    file_data = forms.fields.FileField(required=False)
    text_data = forms.fields.CharField(required=False, widget=forms.Textarea)
    metadata = forms.fields.CharField(required=False, widget=forms.Textarea)

    def clean(self):
        cleaned_data = super().clean()
        fd = cleaned_data.get('file_data')
        td = cleaned_data.get('text_data')
        if not fd and not td:
            raise forms.ValidationError(
                'You must provide either a file or some text')
        if fd and td:
            raise forms.ValidationError(
                'You must provide either a file or some text, not both')


class CreateTimelineAccessTokenForm(forms.ModelForm):
    class Meta:
        model = models.TimelineAccessToken
        fields = [
            'description',
            'permission_read',
            'permission_read_entries',
            'permission_add_entry',
            'permission_edit_entries',
            'permission_clone',
            'script',
            'state',
        ]


class CreateTimelineEntryAccessTokenForm(forms.ModelForm):
    class Meta:
        model = models.TimelineEntryAccessToken
        fields = [
            'description',
            'permission_read',
            'permission_edit',
            'script',
            'state',
        ]
