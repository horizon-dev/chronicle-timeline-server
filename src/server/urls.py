from django.contrib.staticfiles.storage import staticfiles_storage
from django.http import Http404
from django.urls import (include, path, re_path)
from django.views.defaults import page_not_found
from django.views.generic.base import RedirectView

from . import views


urlpatterns = [
    path('api/v1/', include('server.api.v1.urls')),
    re_path(
        '^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('images/favicon.ico'),
            permanent=False),
        name="favicon"),
    re_path(
        '^account/profile/$',
        views.account_profile,
        name='account_profile'),
    re_path(
        '^account/delete/$',
        views.account_delete,
        name="account_delete"),
    re_path(
        '^timeline/(?P<timeline_id>[^/]*)/$',
        views.timeline,
        name='timeline'),
    re_path(
        '^timeline/(?P<timeline_id>[^/]*)/(?P<entry_id>[^/]*)/$',
        views.timeline_entry,
        name='timeline_entry'),
    re_path(
        '^timeline/(?P<timeline_id>[^/]*)/(?P<entry_id>[^/]*)/content/?$',
        views.timeline_entry_content,
        name='timeline_entry_content'),
    re_path(
        '^$',
        views.index,
        name="index"),
    re_path(
        '.*',
        page_not_found,
        {'exception' : Http404()}),
]
