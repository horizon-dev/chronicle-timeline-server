import base64
import io
import re

from django.apps import apps
from django.http import (Http404, StreamingHttpResponse)
from django.urls import (include, re_path)
from django.views.defaults import page_not_found

from rest_framework import (
    decorators,
    exceptions,
    mixins,
    response,
    serializers,
    status,
    viewsets
)
from rest_framework.permissions import IsAuthenticated
from rest_framework_nested import routers

from server import (models, utils)

from . import permissions


class TimelineSerializer(serializers.ModelSerializer):

    class TimelineEntrySerializer(serializers.ModelSerializer):
        class Meta:
            model = models.TimelineEntry
            fields = ('id', 'createdAt',)
            extra_kwargs = {
                'createdAt': {'source': 'created_at'},
            }
            read_only_fields = ('id', 'createdAt')

    entries = TimelineEntrySerializer(many=True, read_only=True)

    def to_representation(self, value):
        r = super(TimelineSerializer, self).to_representation(value)
        if not r['longDescription']:
            r.pop('longDescription')
        return r

    class Meta:
        model = models.Timeline
        fields = (
            'id',
            'createdAt',
            'shortDescription',
            'longDescription',
            'entries',
        )
        extra_kwargs = {
            'createdAt': {'source': 'created_at'},
            'shortDescription': {'source': 'short_description'},
            'longDescription': {'source': 'long_description'},
        }
        read_only_fields = ('id', 'createdAt', 'entries',)


class TimelineListSerializer(serializers.ModelSerializer):

    def to_representation(self, value):
        r = super(TimelineListSerializer, self).to_representation(value)
        if not r['longDescription']:
            r.pop('longDescription')
        return r

    class Meta:
        model = models.Timeline
        fields = (
            'id',
            'createdAt',
            'shortDescription',
            'longDescription',
        )
        extra_kwargs = {
            'createdAt': {'source': 'created_at'},
            'shortDescription': {'source': 'short_description'},
            'longDescription': {'source': 'long_description'},
        }


class TimelineViewSet(
        mixins.CreateModelMixin,
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):

    queryset = models.Timeline.objects.all()

    def get_permissions(self):
        if self.action == 'create':
            return [permissions.DenyPermission()]
        if self.action == 'list':
            return [permissions.DenyPermission()]
        if self.action == 'clone':
            return [permissions.TimelineClonePermission()]
        if self.action == 'retrieve':
            return [permissions.TimelineReadPermission()]
        return [IsAuthenticated()]

    def get_serializer_class(self):
        if self.action == 'list':
            return TimelineListSerializer
        return TimelineSerializer

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return response.Response(
            {'id': serializer.data['id']},
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @decorators.action(detail=True, methods=['post'])
    def clone(self, request, pk=None):
        source = models.Timeline.objects.get(pk=pk)
        self.check_object_permissions(request, source)
        dest = utils.clone_timeline(source, request.user)
        return response.Response(
            {'id': dest.id},
            status=status.HTTP_201_CREATED
        )


class TimelineAccessTokenSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TimelineAccessToken
        fields = (
            'id',
            'secret',
            'description',
            'script',
            'state',
            'created_at',
            'permission_read',
            'permission_read_entries',
            'permission_edit_entries',
            'permission_add_entry',
            'permission_clone',
        )
        extra_kwargs = {
            'secret': {'required': False},
            'allowRead': {'source': 'permission_read'},
            'allowReadEntries': {'source': 'permission_read_entries'},
            'allowEditEntries': {'source': 'permission_edit_entries'},
            'allowAddEntry': {'source': 'permission_add_entry'},
            'allowClone': {'source': 'permission_clone'},
        }
        read_only_fields = ('id', 'created_at',)


class TimelineAccessTokenViewSet(
        mixins.CreateModelMixin,
        viewsets.GenericViewSet):

    serializer_class = TimelineAccessTokenSerializer

    def get_permissions(self):
        return [permissions.DenyPermission()]

    def create(self, request, timeline_pk):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if not hasattr(serializer.validated_data, 'secret'):
            serializer.validated_data['secret'] = utils.generate_secret_key()

        timeline = models.Timeline.objects.get(pk=timeline_pk)
        serializer.save(timeline=timeline)
        headers = self.get_success_headers(serializer.data)
        return response.Response(
            {
                'id': serializer.data['id'],
                'secret': serializer.data['secret'],
            },
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class TimelineEntrySerializer(serializers.ModelSerializer):

    class TimelineEntryMetadataSerializer(serializers.ModelSerializer):
        class Meta:
            model = models.TimelineEntryMetadata
            fields = ('key', 'value')

    metadata = serializers.SerializerMethodField()

    def __init__(self, *args, timeline_pk=None, **kwargs):
        self.timeline_id = timeline_pk
        super(TimelineEntrySerializer, self).__init__(*args, **kwargs)

    def get_metadata(self, obj):
        if hasattr(obj, 'metadata'):
            s = TimelineEntrySerializer.TimelineEntryMetadataSerializer(
                obj.metadata, many=True)
            return s.data
        return []

    def create(self, validated_data):
        timeline = models.Timeline.objects.get(pk=self.timeline_id)
        fs = apps.get_app_config('server').filestore
        writable = False if 'content' in validated_data else True
        te = models.TimelineEntry(
            mime_type=validated_data['mime_type'],
            timeline=timeline,
            previous=timeline.last_entry,
            writable=writable
        )
        te.save()
        if 'content' in validated_data:
            fs.put(timeline.id, te.id, io.BytesIO(validated_data['content']))
        else:
            fs.put(timeline.id, te.id, io.BytesIO(b''))
        if 'metadata' in validated_data:
            for md in validated_data['metadata']:
                tmd = models.TimelineEntryMetadata(
                    key=md['key'],
                    value=md['value'],
                    timeline_entry=te
                )
                tmd.save()
        return te

    def to_internal_value(self, data):
        ret = super().to_internal_value(data)
        errors = {}
        if not 'content' in data:
            if not 'keepOpen' in data:
                errors['content'] = 'Field required'
            elif data['keepOpen'] != True:
                errors['keepOpen'] = 'Must be true if content not set'
        else:
            ret['content'] = base64.b64decode(data['content'])
        if 'metadata' in data:
            ret['metadata'] = data['metadata']
        if errors:
            raise exceptions.ValidationError(errors)
        return ret

    class Meta:
        model = models.TimelineEntry
        fields = ('id', 'createdAt', 'mimeType', 'metadata',)
        extra_kwargs = {
            'createdAt': {'source': 'created_at'},
            'mimeType': {'source': 'mime_type', 'read_only': False},
        }
        depth = 1
        read_only_fields = ('id', 'createdAt',)


class TimelineEntryUpdateSerializer(serializers.ModelSerializer):

    class TimelineEntryMetadataSerializer(serializers.ModelSerializer):
        class Meta:
            model = models.TimelineEntryMetadata
            fields = ('key', 'value')

    metadata = serializers.SerializerMethodField()

    def __init__(self, *args, timeline_pk=None, **kwargs):
        self.timeline_id = timeline_pk
        super().__init__(*args, **kwargs)

    def get_metadata(self, obj):
        if hasattr(obj, 'metadata'):
            s = TimelineEntrySerializer.TimelineEntryMetadataSerializer(
                obj.metadata, many=True)
            return s.data
        return []

    def update(self, instance, validated_data):
        instance.metadata.all().delete()
        for md in validated_data['metadata']:
            tmd = models.TimelineEntryMetadata(
                key=md['key'],
                value=md['value'],
                timeline_entry=instance
            )
            tmd.save()
        return instance

    def to_internal_value(self, data):
        ret = super().to_internal_value(data)
        errors = {}
        if 'metadata' in data:
            ret['metadata'] = data['metadata']
        else:
            errors['metadata'] = 'Field required'
        if errors:
            raise exceptions.ValidationError(errors)
        return ret

    class Meta:
        model = models.TimelineEntry
        fields = ('id', 'createdAt', 'mimeType', 'metadata')
        extra_kwargs = {
            'createdAt': {'source': 'created_at'},
            'mimeType': {'source': 'mime_type'},
        }
        depth = 1
        read_only_fields = ('id', 'createdAt', 'mimeType')


class TimelineEntryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TimelineEntry
        fields = ('id', 'createdAt', 'mimeType')
        extra_kwargs = {
            'createdAt': {'source': 'created_at'},
            'mimeType': {'source': 'mime_type'},
        }
        depth = 1
        read_only_fields = ('id', 'createdAt')


class TimelineEntryViewSet(
        mixins.CreateModelMixin,
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        viewsets.GenericViewSet):

    def get_queryset(self):
        return models.TimelineEntry.objects.filter(
            timeline=self.kwargs['timeline_pk'])

    def get_serializer_class(self):
        if self.action == 'list':
            return TimelineEntryListSerializer
        if self.action == 'update':
            return TimelineEntryUpdateSerializer
        return TimelineEntrySerializer

    def get_permissions(self):
        if (self.action == 'create'
                or self.action == 'put'
                or self.action == 'close'):
            return [permissions.TimelineAddEntryPermission()]
        if self.action == 'list' or self.action == 'retrieve':
            return [permissions.TimelineEntryReadPermission()]
        if self.action == 'update':
            return [permissions.TimelineEntryEditPermission()]
        return [IsAuthenticated()]

    def create(self, request, timeline_pk):
        timeline = models.Timeline.objects.get(pk=timeline_pk)
        self.check_object_permissions(request, timeline)
        s = TimelineEntrySerializer(
            data=request.data,
            timeline_pk=timeline_pk,
            context={'request':request}
        )
        s.is_valid(raise_exception=True)
        self.perform_create(s)
        headers = self.get_success_headers(s.data)
        return response.Response(
            {'id': s.data['id']},
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def update(self, request, timeline_pk, pk=None):
        super().update(request, pk=pk)
        return response.Response(status=status.HTTP_200_OK)

    def list(self, request, timeline_pk):
        timeline = models.Timeline.objects.get(pk=timeline_pk)
        self.check_object_permissions(request, timeline)
        return super().list(self, request)

    @decorators.action(detail=True, methods=['put'])
    def put(self, request, timeline_pk=None, pk=None):
        timeline = models.Timeline.objects.get(pk=timeline_pk)
        self.check_object_permissions(request, timeline)
        if not 'HTTP_CONTENT_RANGE' in request.META:
            return response.Response(
                {'error': 'Content-range header required'},
                status=status.HTTP_400_BAD_REQUEST
            )

        cr = request.META['HTTP_CONTENT_RANGE']
        p = re.compile(r'^\s*bytes\s+(\d+)\s*-\s*(\d+)\s*/\s*[\d+|*]\s*$')
        m = p.match(cr)
        if not m:
            return response.Response(
                {'error': 'Unable to parse Content-range header'},
                status=status.HTTP_400_BAD_REQUEST
            )

        # TODO: Add error checking for ranges
        start_byte = int(m.group(1))
        end_byte = int(m.group(2))
        body = io.BytesIO(request.body)

        entry = models.TimelineEntry.objects.get(pk=pk)

        if not entry.writable:
            return response.Response(
                {'error': 'Entry has been closed'},
                status=status.HTTP_400_BAD_REQUEST
            )

        timeline = entry.timeline
        fs = apps.get_app_config('server').filestore
        fs.put_partial(timeline.id, entry.id, body, start_byte, end_byte)

        return response.Response(status=status.HTTP_200_OK)

    @decorators.action(detail=True, methods=['put'])
    def close(self, request, timeline_pk=None, pk=None):
        timeline = models.Timeline.objects.get(pk=timeline_pk)
        self.check_object_permissions(request, timeline)
        entry = models.TimelineEntry.objects.get(pk=pk)
        entry.writable = False
        entry.save()
        return response.Response(status=status.HTTP_200_OK)


class ContentViewSet(viewsets.ViewSet):

    def get_permissions(self):
        if self.action == 'list':
            return [permissions.TimelineEntryReadPermission()]
        print(self.action)
        return [IsAuthenticated()]

    def list(self, request, entry_pk, **kwargs):
        entry = models.TimelineEntry.objects.get(pk=entry_pk)
        self.check_object_permissions(request, entry)
        timeline = entry.timeline
        fs = apps.get_app_config('server').filestore
        f = fs.get(timeline.id, entry.id)
        return StreamingHttpResponse(f, content_type=entry.mime_type)


class TimelineEntryAccessTokenSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TimelineEntryAccessToken
        fields = (
            'id',
            'secret',
            'description',
            'script',
            'state',
            'created_at',
            'permission_read',
            'permission_edit',
        )
        extra_kwargs = {
            'secret': {'required': False},
            'allowRead': {'source': 'permission_read'},
            'allowEdit': {'source': 'permission_edit'},
        }
        read_only_fields = ('id', 'created_at',)


class TimelineEntryAccessTokenViewSet(
        mixins.CreateModelMixin,
        viewsets.GenericViewSet):

    serializer_class = TimelineEntryAccessTokenSerializer

    def get_permissions(self):
        return [permissions.DenyPermission()]

    def create(self, request, timeline_pk, entry_pk):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if not hasattr(serializer.validated_data, 'secret'):
            serializer.validated_data['secret'] = utils.generate_secret_key()

        entry = models.TimelineEntry.objects.get(pk=entry_pk)
        serializer.save(timeline_entry=entry)
        headers = self.get_success_headers(serializer.data)
        return response.Response(
            {
                'id': serializer.data['id'],
                'secret': serializer.data['secret'],
            },
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


router = routers.SimpleRouter()
router.trailing_slash = '/?'
router.register('timeline', TimelineViewSet)

token_router = routers.NestedSimpleRouter(router, 'timeline', lookup='timeline')
token_router.trailing_slash = '/?'
token_router.register(
    'token',
    TimelineAccessTokenViewSet,
    base_name='timeline-access-token')

entry_router = routers.NestedSimpleRouter(router, 'timeline', lookup='timeline')
entry_router.trailing_slash = '/?'
entry_router.register('entry', TimelineEntryViewSet, base_name='timeline-entry')

content_router = routers.NestedSimpleRouter(
    entry_router,
    'entry',
    lookup='entry')
content_router.trailing_slash = '/?'
content_router.register(
    'content',
    ContentViewSet,
    base_name='timeline-entry-content')

entry_token_router = routers.NestedSimpleRouter(
    entry_router,
    'entry',
    lookup='entry')
entry_token_router.trailing_slash = '/?'
entry_token_router.register(
    'token',
    TimelineEntryAccessTokenViewSet,
    base_name='timeline-entry-access-token')

urlpatterns = [
    re_path('^', include(router.urls)),
    re_path('^', include(token_router.urls)),
    re_path('^', include(entry_router.urls)),
    re_path('^', include(content_router.urls)),
    re_path('^', include(entry_token_router.urls)),
    re_path(
        '.*',
        page_not_found,
        {'exception' : Http404()}),
]
