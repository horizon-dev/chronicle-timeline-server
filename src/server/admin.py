from django import forms
from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from . import models


class UserPermissionsAdmin(admin.ModelAdmin):

    readonly_fields = (
        'user_link',
    )

    list_display = ('username',)

    list_display_links = ('username',)

    def user_link(self, obj):
        link = reverse('admin:auth_user_change', args=(obj.user.id,))
        return format_html(
            '<a href="%s">%s</a>' % (link, obj.user.username))
    user_link.short_description = 'User'

    def username(self, obj):
        return obj.user.username
    username.short_description = 'User'


class UserPublicKeyAdminForm(forms.ModelForm):

    public_key_pem = forms.CharField(
        widget=forms.Textarea,
        required=False
    )

    def __init__(self, *args, **kwargs):
        if 'instance' in kwargs:
            obj = kwargs['instance']
            pem = bytes(obj.public_key).decode('UTF-8')
            if 'initial' in kwargs:
                kwargs['initial']['public_key_pem'] = pem
            else:
                kwargs['initial'] = {'public_key_pem': pem}
        super(UserPublicKeyAdminForm, self).__init__(*args, **kwargs)

    class Meta:
        model = models.UserPublicKey
        fields = ('public_key_pem',)


class UserPublicKeyAdmin(admin.ModelAdmin):

    form = UserPublicKeyAdminForm

    readonly_fields = (
        'user_link',
    )

    list_display = ('username',)

    list_display_links = ('username',)

    def user_link(self, obj):
        link = reverse('admin:auth_user_change', args=(obj.user.id,))
        return format_html(
            '<a href="%s">%s</a>' % (link, obj.user.username))
    user_link.short_description = 'User'

    def username(self, obj):
        return obj.user.username
    username.short_description = 'User'

    def save_model(self, request, obj, form, change):
        obj.public_key = form.cleaned_data['public_key_pem'].encode('UTF-8')
        super(UserPublicKeyAdmin, self).save_model(request, obj, form, change)


class TimelineAdmin(admin.ModelAdmin):

    readonly_fields = (
        'user_link',
        'created_at',
        'last_entry_link',
    )

    list_display = ('id', 'username', 'short_description', 'created_at')

    list_display_links = ('id', 'username',)

    def user_link(self, obj):
        link = reverse('admin:auth_user_change', args=(obj.user.id,))
        return format_html(
            '<a href="%s">%s</a>' % (link, obj.user.username))
    user_link.short_description = 'Owned by'

    def last_entry_link(self, obj):
        link = reverse(
            'admin:server_timelineentry_change',
            args=(obj.last_entry.id,))
        return format_html(
            '<a href="%s">Timeline Entry object (%s)</a>'
            % (link, obj.last_entry.id))
    last_entry_link.short_description = 'Last entry'

    def username(self, obj):
        return obj.user.username
    username.short_description = 'Owned by'


class TimelineEntryMetadataAdmin(admin.StackedInline):
    model = models.TimelineEntryMetadata
    extra = 0


class TimelineEntryAdmin(admin.ModelAdmin):

    readonly_fields = (
        'timeline_link',
        'mime_type',
        'created_at',
        'previous_link',
        'writable',
    )

    inlines = [TimelineEntryMetadataAdmin]

    list_display = ('id', 'timeline_name', 'mime_type', 'created_at')

    list_display_links = ('id', 'timeline_name',)

    def timeline_link(self, obj):
        link = reverse(
            'admin:server_timeline_change',
            args=(obj.timeline.id,))
        return format_html(
            '<a href="%s">%s (%s)</a>'
            % (link, obj.timeline.short_description, obj.timeline.id))
    timeline_link.short_description = 'Part of timeline'

    def previous_link(self, obj):
        link = reverse(
            'admin:server_timelineentry_change',
            args=(obj.previous.id,))
        return format_html(
            '<a href="%s">Timeline Entry object (%s)</a>'
            % (link, obj.previous.id))
    previous_link.short_description = 'Previous entry'

    def timeline_name(self, obj):
        return '{} ({})'.format(obj.timeline.short_description, obj.timeline.id)
    timeline_name.short_description = 'Part of timeline'


class TimelineAccessTokenAdmin(admin.ModelAdmin):

    readonly_fields = (
        'secret',
        'description',
        'timeline_link',
        'created_at',
    )

    list_display = ('id', 'timeline_name', 'description', 'created_at',)

    list_display_links = ('id', 'timeline_name', 'description',)

    def timeline_link(self, obj):
        link = reverse(
            'admin:server_timeline_change',
            args=(obj.timeline.id,))
        return format_html(
            '<a href="%s">%s (%s)</a>'
            % (link, obj.timeline.short_description, obj.timeline.id))
    timeline_link.short_description = 'Part of timeline'

    def timeline_name(self, obj):
        return '{} ({})'.format(obj.timeline.short_description, obj.timeline.id)
    timeline_name.short_description = 'Part of timeline'


class TimelineEntryAccessTokenAdmin(admin.ModelAdmin):

    readonly_fields = (
        'secret',
        'description',
        'timeline_entry_link',
        'created_at',
    )

    list_display = ('id', 'description', 'created_at',)

    list_display_links = ('id', 'description',)

    def timeline_entry_link(self, obj):
        link = reverse(
            'admin:server_timelineentry_change',
            args=(obj.timeline_entry.id,))
        return format_html(
            '<a href="%s">ID: %s</a>'
            % (link, obj.timeline_entry.id))
    timeline_entry_link.short_description = 'Timeline entry'


admin.site.register(models.UserPermissions, UserPermissionsAdmin)
admin.site.register(models.UserPublicKey, UserPublicKeyAdmin)
admin.site.register(models.Timeline, TimelineAdmin)
admin.site.register(models.TimelineEntry, TimelineEntryAdmin)
admin.site.register(models.TimelineAccessToken, TimelineAccessTokenAdmin)
admin.site.register(
    models.TimelineEntryAccessToken, TimelineEntryAccessTokenAdmin)
