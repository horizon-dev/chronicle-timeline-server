import base64
import json
import os

import pyduktape

from django.apps import apps

import allauth.account.adapter

from . import models


class AllauthAccountAdaper(allauth.account.adapter.DefaultAccountAdapter):
    def populate_username(self, request, user):
        user.username = user.email


class JSProxyJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, pyduktape.JSProxy):
            return repr(obj)
        return json.JSONEncoder.default(self, obj)


def generate_secret_key():
    return base64.b32encode(os.urandom(32)).decode('utf8')


def clone_timeline(source, user):
    dest = models.Timeline(
        user=user,
        short_description=source.short_description,
        long_description=source.long_description
    )
    dest.save()

    fs = apps.get_app_config('server').filestore

    for se in source.entries.order_by('pk'):
        de = models.TimelineEntry(
            timeline=dest,
            mime_type=se.mime_type,
        )
        de.save()
        sf = fs.get(source.id, se.id)
        fs.put(dest.id, de.id, sf)

        for smd in se.metadata.order_by('pk'):
            dmd = models.TimelineEntryMetadata(
                timeline_entry=de,
                key=smd.key,
                value=smd.value
            )
            dmd.save()

    return dest
