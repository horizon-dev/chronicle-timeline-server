from abc import (ABC, abstractmethod)

import copy
import json

import pyduktape

from rest_framework import exceptions

import rest_framework.permissions

from server import (models, utils)


class BaseAccessTokenPermission(rest_framework.permissions.BasePermission, ABC):

    def __check_authorization_script(self, script, state):

        authorized = None
        var_state = state
        reject_message = None

        def authorize():
            nonlocal authorized
            if not authorized is None:
                raise Exception
            authorized = True

        def reject(message=None):
            nonlocal authorized, reject_message
            if not authorized is None:
                raise Exception
            authorized = False
            reject_message = message

        def update_state(state):
            nonlocal var_state
            var_state = state

        context = pyduktape.DuktapeContext()
        context.set_globals(
            state=copy.deepcopy(state),
            authorize=authorize,
            reject=reject,
            update_state=update_state
            )
        context.eval_js(script)

        return (authorized, var_state, reject_message)

    def has_permission(self, request, view):
        if hasattr(request, 'access_token'):
            return self.token_permits(request, view, request.access_token)
        return True

    @abstractmethod
    def token_permits(self, request, view, token):
        pass

    def has_object_permission(self, request, view, obj):
        if hasattr(request, 'access_token'):
            at = request.access_token
            p = self.token_permits_object(request, view, obj, at)
            if p:
                script = at.script
                if at.state:
                    state = json.loads(at.state)
                else:
                    state = None
                authd, state, msg = (
                    self.__check_authorization_script(script, state))
                at.state = json.dumps(state, cls=utils.JSProxyJsonEncoder)
                at.save()
                if not authd:
                    raise exceptions.AuthenticationFailed(msg)
        return True

    @abstractmethod
    def token_permits_object(self, request, view, obj, token):
        pass


class DenyPermission(BaseAccessTokenPermission):
    def token_permits(self, request, view, token):
        return False
    def token_permits_object(self, request, view, obj, token):
        return False


class TimelineClonePermission(BaseAccessTokenPermission):

    def token_permits(self, request, view, token):
        return hasattr(token, 'permission_clone') and token.permission_clone

    def token_permits_object(self, request, view, obj, token):
        if isinstance(token, models.TimelineAccessToken):
            return obj.id == token.timeline.id
        return False


class TimelineReadPermission(BaseAccessTokenPermission):

    def token_permits(self, request, view, token):
        return hasattr(token, 'permission_read') and token.permission_read

    def token_permits_object(self, request, view, obj, token):
        if isinstance(token, models.TimelineAccessToken):
            return obj.id == token.timeline.id
        return False


class TimelineAddEntryPermission(BaseAccessTokenPermission):

    def token_permits(self, request, view, token):
        return (hasattr(token, 'permission_add_entry')
                and token.permission_add_entry)

    def token_permits_object(self, request, view, obj, token):
        if isinstance(token, models.TimelineAccessToken):
            return obj.id == token.timeline.id
        return False


class TimelineEntryReadPermission(BaseAccessTokenPermission):

    def token_permits(self, request, view, token):
        if isinstance(token, models.TimelineAccessToken):
            return (hasattr(token, 'permission_read_entries')
                    and token.permission_read_entries)
        if isinstance(token, models.TimelineEntryAccessToken):
            return (hasattr(token, 'permission_read')
                    and token.permission_read)
        return False

    def token_permits_object(self, request, view, obj, token):
        if isinstance(token, models.TimelineAccessToken):
            if isinstance(obj, models.Timeline):
                return obj.id == token.timeline.id
            if isinstance(obj, models.TimelineEntry):
                return obj.timeline.id == token.timeline.id
        if isinstance(token, models.TimelineEntryAccessToken):
            if isinstance(obj, models.Timeline):
                return obj.id == token.timeline_entry.timeline.id
            if isinstance(obj, models.TimelineEntry):
                return obj.id == token.timeline_entry.id
        return False


class TimelineEntryEditPermission(BaseAccessTokenPermission):

    def token_permits(self, request, view, token):
        if isinstance(token, models.TimelineAccessToken):
            return (hasattr(token, 'permission_edit_entries')
                    and token.permission_edit_entries)
        if isinstance(token, models.TimelineEntryAccessToken):
            return (hasattr(token, 'permission_edit')
                    and token.permission_edit)
        return False

    def token_permits_object(self, request, view, obj, token):
        if isinstance(token, models.TimelineAccessToken):
            if isinstance(obj, models.Timeline):
                return obj.id == token.timeline.id
            if isinstance(obj, models.TimelineEntry):
                return obj.timeline.id == token.timeline.id
        if isinstance(token, models.TimelineEntryAccessToken):
            if isinstance(obj, models.Timeline):
                return obj.id == token.timeline_entry.timeline.id
            if isinstance(obj, models.TimelineEntry):
                return obj.id == token.timeline_entry.id
        return False
