import time

from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.backends import openssl
from cryptography.hazmat.primitives import serialization

from django.conf import settings
from django.contrib import auth
from django.core.cache import caches

from rest_framework import (authentication, exceptions)

from chronicle.auth import v1

from server import models


class Authentication(authentication.BaseAuthentication):

    def __get_url(self, request):
        if 'HTTP_X_FORWARDED_PROTO' in request.META:
            scheme = request.META['HTTP_X_FORWARDED_PROTO']
        else:
            scheme = request.scheme
        return '{}://{}{}'.format(
            scheme,
            request.META['HTTP_HOST'],
            request.path)

    def __get_query(self, request):
        query = []
        for k in request.GET:
            for v in request.GET.getlist(k):
                query.append((k, v))
        return query

    def __get_parts(self, hdr, auth_method):
        def to_pair(e):
            e = e.strip()
            p = e.split('=', 1)
            k = p[0]
            v = p[1].replace('"', '')
            return (k, v)

        return dict([to_pair(e) for e in \
                hdr[len(auth_method):].strip().split(',') \
                if not e.strip() == ''])

    def __check_common(self, parts):
        # Check version
        if parts['auth_version'] != '1.0':
            raise exceptions.AuthenticationFailed(
                'Invalid authorization version')

        # Check signature method
        if parts['auth_signature_method'] != 'RSA-SHA512':
            raise exceptions.AuthenticationFailed('Invalid signature method')

        # Check the timestamp is bounds
        seconds = settings.TIMELINE_SERVER_AUTH_VALID_TIME * 60
        current_time = int(time.time())
        request_time = int(parts['auth_timestamp'])
        if request_time < (current_time - seconds) \
                or request_time >= (current_time + seconds):
            raise exceptions.AuthenticationFailed('Invalid timestamp')

        # Check that the nonce hasn't already been used
        cache = caches['v1_api_nonce']
        if 'auth_username' in parts:
            key = '{}_{}'.format(parts['auth_username'], parts['auth_nonce'])
        else:
            key = '{}_{}'.format(parts['auth_token_id'], parts['auth_nonce'])
        if cache.get(key):
            raise exceptions.AuthenticationFailed('Invalid nonce')
        cache.set(key, 'used', seconds)

    def __authenticate_token(self, request, hdr, auth_method):
        parts = self.__get_parts(hdr, auth_method)
        self.__check_common(parts)

        try:
            if parts['auth_token_id'].startswith('te'):
                access_token = models.TimelineEntryAccessToken.objects.get(
                    pk=parts['auth_token_id'].replace('te', ''))
                user = access_token.timeline_entry.timeline.user
            else:
                access_token = models.TimelineAccessToken.objects.get(
                    pk=parts['auth_token_id'].replace('t', ''))
                user = access_token.timeline.user
        except:
            raise exceptions.AuthenticationFailed('Invalid token ID')

        try:
            v1.verify_hmac_sha512_signature(
                access_token.secret,
                parts['auth_signature'],
                request.method,
                self.__get_url(request),
                self.__get_query(request),
                parts['auth_token_id'],
                request.body,
                nonce=parts['auth_nonce'],
                timestamp=parts['auth_timestamp']
            )
        except InvalidSignature:
            raise exceptions.AuthenticationFailed('Invalid signature')

        request.access_token = access_token

        return (user, None)

    def __authenticate_user(self, request, hdr, auth_method):
        parts = self.__get_parts(hdr, auth_method)
        # Check that the user exists
        try:
            um = auth.get_user_model()
            user = um.objects.get(username=parts['auth_username'])
        except um.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')
        # Verify the signature
        pub_key = serialization.load_pem_public_key(
            bytes(user.public_key.public_key),
            backend=openssl.backend
        )
        try:
            v1.verify_rsa_sha512_signature(
                pub_key,
                parts['auth_signature'],
                request.method,
                self.__get_url(request),
                self.__get_query(request),
                parts['auth_username'],
                request.body,
                parts['auth_nonce'],
                parts['auth_timestamp']
            )
        except InvalidSignature:
            raise exceptions.AuthenticationFailed('Invalid signature')

        return (user, None)

    def authenticate(self, request):
        hdr = request.META.get('HTTP_AUTHORIZATION')
        if not hdr:
            raise exceptions.AuthenticationFailed('Authorization is required')

        user_auth_method = 'chronicle-user-hmac'
        token_auth_method = 'chronicle-token-hmac'
        if hdr.lower().startswith(user_auth_method):
            return self.__authenticate_user(request, hdr, user_auth_method)
        elif hdr.lower().startswith(token_auth_method):
            return self.__authenticate_token(request, hdr, token_auth_method)

        raise exceptions.AuthenticationFailed(
            'Authentication method not understood')
